[{
  subject_name: 'Organisation and IT',
  year_of_study: 1,
  teacher_id: '1AVACHM',
  icon: 'mdi-database',
  module_code: 'sx10'
}, {
  subject_name: 'Introduction to Programming',
  year_of_study: 1,
  teacher_id: '1DG1AC',
  icon: 'mdi-chip',
  module_code: 'sx11'
}, {
  subject_name: 'Curiosity',
  year_of_study: 1,
  teacher_id: '1DG2AC',
  icon: 'mdi-cloud-sync',
  module_code: 'sx12'

}, {
  subject_name: 'Databases',
  year_of_study: 1,
  teacher_id: '1DV0LD',
  icon: 'mdi-contrast-circle',
  module_code: 'sx13'
}, {
  subject_name: 'Introduction to Infrastructures',
  year_of_study: 2,
  teacher_id: '1DV10LD',
  icon: 'mdi-cookie',
  module_code: 'sx14'
}, {
  subject_name: "Project IT's in the Game",
  year_of_study: 2,
  teacher_id: '1DV11LD',
  icon: 'mdi-desktop-mac',
  module_code: 'sx15'
}, {
  subject_name: 'Object Oriented Programming',
  year_of_study: 2,
  teacher_id: '1DV12LD',
  icon: 'mdi-drawing',
  module_code: 'sx16'
}, {
  subject_name: 'Computer Networks',
  year_of_study: 2,
  teacher_id: '1DV13LD',
  icon: 'mdi-database',
  module_code: 'sx17'
}, {
  subject_name: 'Imagine 2030',
  year_of_study: 3,
  teacher_id: '1DV14LD',
  icon: 'mdi-chip',
  module_code: 'sx18'
}, {
  subject_name: 'Web Applications',
  year_of_study: 3,
  teacher_id: '1DV15LD',
  icon: 'mdi-cloud-sync',
  module_code: 'sx19'
}, {
  subject_name: 'Requirements Engineering',
  year_of_study: 3,
  teacher_id: '1DV1LD',
  icon: 'mdi-contrast-circle',
  module_code: 'sx20'
}, {
  subject_name: 'Project team Building Challenge',
  year_of_study: 3,
  teacher_id: '1DV2LD',
  icon: 'mdi-cookie',
  module_code: 'sx21'
}]
