const uploadFile = require("../src/upload");
const fs = require("fs");

const upload = async (req, res) => {
    try {
        await uploadFile(req, res);

        if (req.file === undefined) {
            return res.status(400).send({message: "Please upload a file!"});
        }

        //show incoming file details
        console.log(req.file)

        // accepted files // keith
        let acceptedFiles = ["jpg", "png", "pdf", "txt", "zip", "rar", "doc", "ocx",
        "xls","lsx"]

        // reject if file not acceptable format // keith
        let accepted = false;
        acceptedFiles.forEach((el) => {
            if (req.file.filename.substr(-3) === el) {
                accepted = true
            }
        })

        // reject if file not acceptable format // keith
        if (accepted === false) {
            // show incoming file format
            return res.status(400).send({message: "This format is not allowed!"});
        }

        res.status(200).send({
            message: "Uploaded: " + req.file.originalname,
        });
    } catch (err) {
        console.log("file too big")
        res.status(500).send({
            message: `Could not upload the file. Check filesize. ${err}`,
        });
    }



};

const getListFiles = (req, res) => {
    const directoryPath = __basedir + "/uploads/";

    fs.readdir(directoryPath, function (err, files) {
        if (err) {
            res.status(500).send({
                message: "Unable to scan files!",
            });
        }

        let fileInfos = [];

        files.forEach((file) => {
            fileInfos.push({
                name: file,
                url: __basedir +"/uploads/"+ file,
            });
        });

        res.status(200).send(fileInfos);
    });
};

const download = (req, res) => {
    const fileName = req.params.name;
    const directoryPath = __basedir + "/uploads/";

    res.download(directoryPath + fileName, fileName, (err) => {
        if (err) {
            res.status(500).send({
                message: "Could not download the file. " + err,
            });
        }
    });
};

module.exports = {
    upload,
    getListFiles,
    download,
};