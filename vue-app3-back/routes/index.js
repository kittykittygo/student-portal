const express = require('express');
const router = express.Router();

const controller = require("../src/file.controller");

// file upload routes by keith
router.post("/upload", controller.upload);
router.get("/files", controller.getListFiles);
router.get("/files/:name", controller.download);

/* GET home page. */
router.get('/', function (req, res, next) {
    res.send({message: 'Student portal backend server'});
});

/* GET home page. */
router.get('/favicon.ico', function (req, res, next) {
    res.send({});
});

module.exports = router;
